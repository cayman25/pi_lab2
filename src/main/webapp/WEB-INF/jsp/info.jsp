<!DOCTYPE html>
<html>
<head>
    <title>Lab2</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link href="/css/main.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>

<body>
<header>
    <h2>Lab 2</h2>
</header>

<nav>
    <ul>
        <li><a href="/student/list">Studenci</a></li>
        <li><a href="/grupa/list">Grupy</a></li>
        <li><a href="/specjalizacja/list">Specjalizacje</a></li>
    </ul>
</nav>

<main>
    <article>
        <form method="post" >
        <c:forEach items="${students}" var="student">
            <h1>Student ${student.imie} ${student.nazwisko} nalezy do grup:</h1>
        </c:forEach>
        <c:forEach items="${studentGroups}" var="studentGroup">
            <h1>- ${studentGroup.nazwa_g} ${studentGroup.nazwa_s}</h1>

        </c:forEach>
            <h5> Wybierz grupe i dodaj</h5>

        <select class="form-control" id="sel1" name="grupa">
            <c:forEach items="${groups}" var="group">
                <option value="${group.id_g}">Nazwa grupy: ${group.nazwa_g} Nazwa specjalizacji:${group.nazwa_s} </option>
            </c:forEach>
        </select>
                <button type="submit" class="btn btn-success">Add</button>
        </form>
        <br>
    </article>
</main>
<footer>
    Mariusz Drobot Z611
</footer>
</body>
</html>