<!DOCTYPE html>
<html>
<head>
    <title>Lab2</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link href="/css/main.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>

<body>
<header>
    <h2>Lab 2</h2>
</header>

<nav>
    <ul>
        <li><a href="/student/list">Studenci</a></li>
        <li><a href="/grupa/list">Grupy</a></li>
        <li><a href="/specjalizacja/list">Specjalizacje</a></li>
    </ul>student.jsp
</nav>

<main>
    <article>
            <c:forEach items="${groups}" var="group">
                <form method="post">
                      <div id="divStudent">
                          <div class="form-group">
                                <label for="exampleInputEmail1">Nazwa grupy</label>
                                <input type="text"  value="${group.nazwa_g}" name="nazwa" class="form-control" id="exampleInputEmail1" placeholder="Wprowadz nazwe">
                           </div>
                          <div class="form-group">
                                <label for="exampleInputEmail1">ID specjalizacji</label>
                                <input type="text"  value="${group.id_spec}" name="id_spec" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Wprowadz id spec">
                          </div>
                          <div class="form-group">
                                <label for="exampleInputEmail1">Rok Akademicki</label>
                                <input type="text"  value="${group.rok_akad}" name="rok_akad" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Wprowadz rok akademicki">
                          </div>
                          <button type="submit" class="btn btn-success">Save</button>
                      </div>
                 </form>
            </c:forEach>
    </article>
</main>
<footer>
    Mariusz Drobot Z611
</footer>
</body>
</html>