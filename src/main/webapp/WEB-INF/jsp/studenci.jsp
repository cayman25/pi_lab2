<!DOCTYPE html>
<html>
<head>
    <title>Lab2</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link href="/css/main.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>

<body>
<header>
    <h2>Lab 2</h2>
</header>

<nav>
    <ul>
        <li><a href="/student/list">Studenci</a></li>
        <li><a href="/grupa/list">Grupy</a></li>
        <li><a href="/specjalizacja/list">Specjalizacje</a></li>
    </ul>
</nav>

<main>
    <article>
        <div id="title"> Lista studentow <br> </div>
        <c:forEach items="${students}" var="student">
            <div id="divStudent">
                    ${student.id_s}
                    ${student.nazwisko}
                    ${student.imie}
                    ${student.data_u}
                    ${student.miasto}
            </div>
            <div id="mngMenu">
                <a href="/student/dodajStudenta">
                    <button type="button" class="btn btn-success">Add</button>
                </a>
                <a href="/student/usun/${student.id_s}">
                    <button type="button" class="btn btn-danger">Delete</button>
                </a>
                <a href="/student/${student.id_s}">
                    <button type="button" class="btn btn-primary">Modify</button>
                </a>
                <a href="/student/${student.id_s}/info">
                    <button type="button" class="btn btn-info">Info</button>
                </a>
            </div>
            <br>
        </c:forEach>
    </article>
</main>
<footer>
    Mariusz Drobot Z611
</footer>
</body>
</html> 