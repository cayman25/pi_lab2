package com.example.demo.config;

import org.springframework.stereotype.Service;

import java.sql.*;

@Service
public class DatabaseConnector {

    String url = "jdbc:mysql://projektyindywidualnymysql.ctbzldhob7ii.us-east-2.rds.amazonaws.com/students";
    String user = "root";
    String pass  = "***";
    Connection con;


    public ResultSet connectToDbAndMakeQuery(String query) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection(url, user, pass);
            return con.createStatement().executeQuery(query);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public  int connectToDvAndMakeInsert(String query){
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection(url, user, pass);
            return con.createStatement().executeUpdate(query);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return 0;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return 0;
        }
    }
}
