package com.example.demo.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class Group {

    private int Id_g;
    private String nazwa_g;
    private int Id_spec;
    private String spec_nazwa;
    private String Rok_akad;

    public Group(int id_g, String nazwa_g, String spec_nazwa, String rok_akad) {
        this. Id_g = id_g;
        this.nazwa_g = nazwa_g;
        this.spec_nazwa = spec_nazwa;
        this.Rok_akad = rok_akad;
    }

    public Group(int id_g, String nazwa_g, int id_spec, String rok_akad) {
        this.Id_g = id_g;
        this.nazwa_g = nazwa_g;
        this.Id_spec = id_spec;
        this.Rok_akad = rok_akad;
    }
}

