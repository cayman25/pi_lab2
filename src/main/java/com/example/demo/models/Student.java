package com.example.demo.models;

import lombok.*;
import org.springframework.stereotype.Service;

@Getter
@Setter
public class Student {

    private int id_s;
    private String nazwisko;
    private String imie;
    private String data_u;
    private String miasto;


    public Student(String nazwisko, String imie, String data_u, String miasto) {
        this.nazwisko = nazwisko;
        this.imie = imie;
        this.data_u = data_u;
        this.miasto = miasto;
    }

    public Student(int id_s, String nazwisko, String imie, String data_u, String miasto) {
        this.id_s = id_s;
        this.nazwisko = nazwisko;
        this.imie = imie;
        this.data_u = data_u;
        this.miasto = miasto;
    }
}
