package com.example.demo.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class StudentGroupsDTO{

    private int id_g;
    private String nazwa_g;
    private int id_spec;
    private String nazwa_s;
    private String data;
    private String rok_akad;

    public StudentGroupsDTO(int id_g, String nazwa_g) {
        this.id_g = id_g;
        this.nazwa_g = nazwa_g;
    }

    public StudentGroupsDTO(int id_g, String nazwa_g, String nazwa_s) {
        this.id_g = id_g;
        this.nazwa_g = nazwa_g;
        this.nazwa_s = nazwa_s;
    }
}