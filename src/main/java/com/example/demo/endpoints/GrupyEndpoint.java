package com.example.demo.endpoints;

import com.example.demo.config.DatabaseConnector;
import com.example.demo.models.Group;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value="/grupa")
public class GrupyEndpoint {

    @Autowired
    DatabaseConnector databaseConnector;

    List<Group> getGrupyFromResultSet(ResultSet rs){
        List<Group> groups= new ArrayList<>();

        while(true){
            try {
                if (!rs.next()) break;
            } catch (SQLException e) {
                e.printStackTrace();

            }
            try {
                groups.add(new Group(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4)));
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return groups;
    }


    @GetMapping("/list")
    public String getAllGroups (Model model) throws SQLException {
        ResultSet rs = databaseConnector.connectToDbAndMakeQuery("select Id_g, Nazwa_g, Nazwa_s, Rok_akad from Grupy g join Specjalizacje s on g.Id_spec=s.id_spec");
        List<Group> groups = new ArrayList<>();

        while(rs.next()){
            groups.add(new Group(rs.getInt(1), rs.getString(2),rs.getString(3),rs.getString(4)));
        }
        model.addAttribute("groups", groups);
        return "grupy";
    }

    @GetMapping("/dodaj")
    public String dodajGrupe (Model model){
        ResultSet rs = databaseConnector.connectToDbAndMakeQuery("Select * from Specjalizacje");
        model.addAttribute("specializations", SpecjalizacjeEndpoint.getSpecializationsFromResultSet(rs));
        return "dodajGrupe";
    }

    @PostMapping("/dodaj")
    public String dodajGrupePost(
            @RequestParam("nazwa")String nazwa,
            @RequestParam("specjalizacja")int specjalizacja,
            @RequestParam("rok_akad")String rok_akad){

        databaseConnector.connectToDvAndMakeInsert("Insert into Grupy (Nazwa_g,Id_spec,Rok_akad) Values ('" + nazwa +"',"+ specjalizacja +","+rok_akad+")");
        return "redirect:/grupa/list";
    }

    @GetMapping("/{id}")
    public String getSpecjalizationById(Model model,
                                        @PathVariable("id") int id) throws SQLException {
        ResultSet rs =  databaseConnector.connectToDbAndMakeQuery("Select * from Grupy where Id_g =" + id);
        model.addAttribute("groups", getGrupyFromResultSet(rs));
        return "grupa";
    }

    @PostMapping("/{id}")
    public String modifySpecializationById(
            @PathVariable("id") int id,
            @RequestParam("nazwa")String nazwa,
            @RequestParam("id_spec")int id_spec,
            @RequestParam("rok_akad")String rok_akad){
        databaseConnector.connectToDvAndMakeInsert("Update Grupy set nazwa_g='"+ nazwa+ "', Id_spec=" + id_spec + ", rok_akad="+rok_akad + " where id_spec="+id);
        return "redirect:/grupa/list";
    }

    @GetMapping("/usun/{id}")
    public String deleteSpecializationById(
            @PathVariable("id") int id){
        databaseConnector.connectToDvAndMakeInsert("Delete from Grupy where id_g=" +id);
        return "redirect:/grupa/list";
    }
}
