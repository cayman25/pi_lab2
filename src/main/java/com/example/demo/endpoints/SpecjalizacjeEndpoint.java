package com.example.demo.endpoints;

import com.example.demo.config.DatabaseConnector;
import com.example.demo.models.Specialization;
import com.example.demo.models.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value="/specjalizacja")
public class SpecjalizacjeEndpoint {

    @Autowired
    DatabaseConnector databaseConnector;

    public static List<Specialization> getSpecializationsFromResultSet(ResultSet rs){
        List<Specialization> specializations = new ArrayList<>();
        while(true){
            try {
                if (!rs.next()) break;
            } catch (SQLException e) {
                e.printStackTrace();

            }
            try {
                specializations.add(new Specialization(rs.getInt(1), rs.getString(2)));
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return specializations;
    }

    @GetMapping({"/list"})
    public String getAllSpecializations(Model model) throws SQLException {
        ResultSet rs = databaseConnector.connectToDbAndMakeQuery("Select * from Specjalizacje");
        model.addAttribute("specializations", getSpecializationsFromResultSet(rs));
        return "specjalizacje";
    }

    @GetMapping("/dodaj")
    public String dodajSpecjalizacje (){
        return "dodajSpecjalizacje";
    }

    @PostMapping("/dodaj")
    public String dodajSpecjalizacjePost(
                                   @RequestParam("nazwa")String nazwa){
        databaseConnector.connectToDvAndMakeInsert("Insert into Specjalizacje (nazwa_s) Values ('" + nazwa + "')");
        return "redirect:/specjalizacja/list";
    }

    @GetMapping("/{id}")
    public String getSpecjalizationById(Model model,
                                   @PathVariable("id") int id) throws SQLException {
        ResultSet rs =  databaseConnector.connectToDbAndMakeQuery("Select * from Specjalizacje where Id_spec =" + id);
        model.addAttribute("specializations", getSpecializationsFromResultSet(rs));
        return "specjalizacja";
    }

    @PostMapping("/{id}")
    public String modifySpecializationById(
                                        @PathVariable("id") int id,
                                        @RequestParam("nazwa")String nazwa){
        databaseConnector.connectToDvAndMakeInsert("Update Specjalizacje set nazwa_s='"+ nazwa+ "'where id_spec="+id);
        return "redirect:/specjalizacja/list";
    }

    @GetMapping("/usun/{id}")
    public String deleteSpecializationById(
                                        @PathVariable("id") int id){
        databaseConnector.connectToDvAndMakeInsert("Delete from Specjalizacje where id_spec=" +id);
        return "redirect:/specjalizacja/list";
    }



}
