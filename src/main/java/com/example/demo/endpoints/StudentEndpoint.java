package com.example.demo.endpoints;

import com.example.demo.config.DatabaseConnector;
import com.example.demo.models.Group;
import com.example.demo.models.Student;
import com.example.demo.models.StudentGroupsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


@Controller
@RequestMapping(value = "/student")
public class StudentEndpoint {

    @Autowired
    DatabaseConnector databaseConnector;

    List<Student> getStudentFromResultSet(ResultSet rs) {
        List<Student> students = new ArrayList<>();

        while (true) {
            try {
                if (!rs.next()) break;
            } catch (SQLException e) {
                e.printStackTrace();

            }
            try {
                students.add(new Student(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getDate(4).toString(), rs.getString(5)));
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return students;
    }

    List<StudentGroupsDTO>getGroupFromResultSet(ResultSet rs){

        List<StudentGroupsDTO> groups = new ArrayList<>();
        while(true){
            try {
                if (!rs.next()) break;
            } catch (SQLException e) {
                e.printStackTrace();

            }
            try {
                groups.add(new StudentGroupsDTO(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4), null, rs.getString(5)));
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return groups;
    }

    List<StudentGroupsDTO>getUserGroupFromResultSet(ResultSet rs){

        List<StudentGroupsDTO> userGroups = new ArrayList<>();
        while(true){
            try {
                if (!rs.next()) break;
            } catch (SQLException e) {
                e.printStackTrace();

            }
            try {
                userGroups.add(new StudentGroupsDTO(rs.getInt(1), rs.getString(2),rs.getString(3)));
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return userGroups;
    }

    @GetMapping("/list")
    public String getAllStudents (Model model) throws SQLException {
        ResultSet rs = databaseConnector.connectToDbAndMakeQuery("select * from Studenci");
        model.addAttribute("students",getStudentFromResultSet(rs));
        return "studenci";
    }

    @GetMapping("/dodajStudenta")
    public String dodajStudenta (){
        return "dodajStudenta";
    }

    @PostMapping("/dodajStudenta")
    public String dodajStudentaPost(
                            @RequestParam("imie")String imie,
                            @RequestParam("nazwisko")String nazwisko,
                            @RequestParam("data_u")String data_u,
                            @RequestParam("miasto")String miasto){
        databaseConnector.connectToDvAndMakeInsert("Insert into Studenci (nazwisko,imie,data_u,miasto) Values ('" + nazwisko + "', '" + imie + "', '" + data_u + "', '" +miasto + "')");
        return "redirect:/student/list";
    }

    @GetMapping("/{id}")
    public String getStudentById(Model model,
                                 @PathVariable("id") int id) throws SQLException {
        ResultSet rs =  databaseConnector.connectToDbAndMakeQuery("Select * from Studenci where Id_s =" + id);
        model.addAttribute("students", getStudentFromResultSet(rs));
            return "student";
    }

    @PostMapping("/{id}")
    public String modifyStudentById(
                                    @PathVariable("id") int id,
                                    @RequestParam("imie")String imie,
                                    @RequestParam("nazwisko")String nazwisko,
                                    @RequestParam("data_u")String data_u,
                                    @RequestParam("miasto")String miasto){
        databaseConnector.connectToDvAndMakeInsert("Update Studenci set nazwisko='"+nazwisko+"', imie='"+imie+"', data_u='"+data_u+"', miasto='"+miasto+"' where id_s="+id);
        return "redirect:/student/list";
    }

    @GetMapping("/usun/{id}")
    public String deleteStudentById(
                                  @PathVariable("id") int id){
        databaseConnector.connectToDvAndMakeInsert("Delete from Studenci where id_s=" +id);
        return "redirect:/student/list";
    }

    @GetMapping("/{id}/info")
    public String getInfoAboutStudent(Model model,
                                      @PathVariable("id") int id) throws SQLException {
        ResultSet rs =  databaseConnector.connectToDbAndMakeQuery("Select * from Studenci where Id_s =" + id);
        model.addAttribute("students", getStudentFromResultSet(rs));
        rs = databaseConnector.connectToDbAndMakeQuery("SELECT g.Id_g, g.Nazwa_g, s.Id_spec, s.Nazwa_s, g.rok_akad from Grupy g join Specjalizacje s on s.Id_spec=g.Id_spec ");
        model.addAttribute("groups", getGroupFromResultSet(rs));
        rs = databaseConnector.connectToDbAndMakeQuery("SELECT g.Id_g, g.Nazwa_g, s.Nazwa_s from Grupy g join Student_Grupa sg on sg.Id_g=g.Id_g join Specjalizacje s on s.Id_spec=g.Id_spec where Id_s="+id);
        model.addAttribute( "studentGroups", getUserGroupFromResultSet(rs));
        return "info";
    }

    @PostMapping("/{id}/info")
    public String postAddToGroup(@PathVariable("id") int id,
                                 @RequestParam("grupa")int id_grupy){
        databaseConnector.connectToDvAndMakeInsert("Insert into Student_Grupa (Id_s,Id_g,Data) Values ("+id+","+id_grupy+",2019)");
        return "redirect:/student/{id}/info";
    }


}
